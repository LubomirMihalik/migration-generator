# ORM generátor migrací
- formátuje dotazy
- lze použít filter na tabulku (grepne výstup)
- přidává GRANT ALL na sekvence.

## How to run
- set `migration-generator` diretory to .gitignore_global
- download this repository into `migration-generator` directory in your project
- Add alias to your `.profile`
````
function migrationGenerate () {
	clear
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml exec php sh -c "php migration-generator/migrationGenerate.php $1"
}
````
- Run `. ~/.profile`
- Finally run `migrationGenerate` or `migrationGenerate <filter>` in your directory project with git

