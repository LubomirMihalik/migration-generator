<?php

use Doctrine\SqlFormatter\NullHighlighter;
use Doctrine\SqlFormatter\SqlFormatter;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Debug\Debug;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require __DIR__ . '/../app/autoload.php';

//Debug::enable();

require_once __DIR__ . '/../app/AppKernel.php';
require_once __DIR__ . '/../app/AppEnvironment.php';
require_once __DIR__ . '/MigrationService.php';

$kernel = new AppKernel('dev', true);
$kernel->boot();

/** @var \Symfony\Component\DependencyInjection\Container $container */
$container = $kernel->getContainer();

echo 'Migrations generator started...' . PHP_EOL;

$inputDefinition = new InputDefinition();
$inputDefinition->addArgument(new InputArgument('filter', InputArgument::OPTIONAL, 'for grep queries'));
$input = new ArgvInput(null, $inputDefinition);
$filter = $input->getArgument('filter');

$em = $container->get('doctrine.orm.default_entity_manager');
$migrationService = new MigrationService($em, new SqlFormatter(new NullHighlighter()));
$queries = $migrationService->getQueries();

echo 'Queries count=' . count($queries) . PHP_EOL;

if (count($queries) === 0) {
    echo 'Database schema is satisfying ORM, no migrations was generated.' . PHP_EOL;
    die();
}

if ($filter) {
    $filteredQueries = $migrationService->filter($queries, $filter);
    echo 'Filtered queries by filter="' . $filter . '" count=' . count($filteredQueries) . PHP_EOL;
} else {
    $filteredQueries = $queries;
}

if (count($filteredQueries) === 0) {
    echo 'It is nothing to generate, try clear cache or use another filter.' . PHP_EOL;
    die();
}

$fileName = $migrationService->generate($filteredQueries, __DIR__ . '/../migrations/DoctrineMigrations/');
if ($fileName === false) {
    echo 'Migration file could not be saved.' . PHP_EOL;
    die();
}

echo sprintf('Migration file "%s" was saved.', realpath($fileName)) . PHP_EOL;
