<?php

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\SqlFormatter\SqlFormatter;
use Nette\Utils\Strings;

class MigrationService
{
    private const LINE_LENGTH_LIMIT = 100;
    private const INDENT_CHARACTERS = '    ';
    private const INDENT_COUNT = 3;

    private const TEMPLATE = '<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class {{ migrationClassName }} extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // dát vědět BI, že se mění databáze
        {{ queries }}
    }

    public function down(Schema $schema): void
    {
    }
}
';

    private EntityManagerInterface $entityManager;
    private SqlFormatter $sqlFormatter;

    public function __construct(EntityManager $entityManager, SqlFormatter $sqlFormatter)
    {
        $this->entityManager = $entityManager;
        $this->sqlFormatter = $sqlFormatter;
    }

    public function getQueries(): array
    {
        $tool = new SchemaTool($this->entityManager);

        return $tool->getUpdateSchemaSql($this->entityManager->getMetadataFactory()->getAllMetadata());
    }

    public function filter(array $queries, string $filter): array
    {
        $data = new \Doctrine\Common\Collections\ArrayCollection($queries);

        return $data->filter(function (string $query) use ($filter) {
            return Strings::contains($query, $filter);
        })->toArray();
    }

    public function generate(array $sqlCommands, string $migrationDirectory)
    {
        $additionalCommands = []; //$this->findAdditionalCommands($sqlCommands);
        $sqlCommands = array_merge($sqlCommands, $additionalCommands);
        $formattedSqlCommands = $this->formatSqlCommandsIfLengthOverflow($sqlCommands);
        $escapedFormattedSqlCommands = $this->escapeSqlCommands($formattedSqlCommands);
        $migrationClassName = 'Version' . date('YmdHis');
        $migrationFileRawData = str_replace(
            ['{{ migrationClassName }}', '{{ queries }}'],
            [
                $migrationClassName,
                implode(PHP_EOL . self::INDENT_CHARACTERS . self::INDENT_CHARACTERS, $escapedFormattedSqlCommands),
            ],
            self::TEMPLATE
        );

        echo $migrationFileRawData;

        $migrationFilePath = $migrationDirectory . $migrationClassName . '.php';
        $writtenBytes = file_put_contents($migrationFilePath, $migrationFileRawData);

        return $writtenBytes === false ? false : $migrationFilePath;
    }

    /**
     * @param string[] $sqlCommands
     *
     * @return string[]
     */
    private function findAdditionalCommands(array $sqlCommands): array
    {
        $additionalCommands = [];
        foreach ($sqlCommands as $sqlCommand) {
            $matches = null;
            if (preg_match('/CREATE TABLE ([a-z_]*) \(/', $sqlCommand, $matches)) {
                if (Strings::contains($sqlCommand, 'SERIAL')) {
                    $additionalCommands[] = sprintf('GRANT ALL PRIVILEGES ON SEQUENCE %s_id_seq TO GROUP pgs_seduo_rw', $matches[1]);
                }
                $additionalCommands[] = sprintf('GRANT ALL PRIVILEGES ON TABLE %s TO GROUP pgs_seduo_rw', $matches[1]);
                $additionalCommands[] = sprintf('ALTER TABLE %s OWNER TO lmc', $matches[1]);
            }
        }

        return $additionalCommands;
    }

    /**
     * @param string[] $filteredSchemaDiffSqlCommands
     *
     * @return string[]
     */
    private function formatSqlCommandsIfLengthOverflow(array $filteredSchemaDiffSqlCommands): array
    {
        $formattedSqlCommands = [];
        foreach ($filteredSchemaDiffSqlCommands as $key => $filteredSchemaDiffSqlCommand) {
            if (strlen($filteredSchemaDiffSqlCommand) > self::LINE_LENGTH_LIMIT) {
                $formattedSqlCommands[] = $this->formatSqlCommand($filteredSchemaDiffSqlCommand);
            } else {
                $formattedSqlCommands[] = $filteredSchemaDiffSqlCommand;
            }
        }

        return $formattedSqlCommands;
    }

    private function formatSqlCommand(string $filteredSchemaDiffSqlCommand): string
    {
        $formattedQuery = $this->sqlFormatter->format($filteredSchemaDiffSqlCommand, self::INDENT_CHARACTERS);
        $formattedQueryLines = array_map('rtrim', explode("\n", $formattedQuery));

        return "\n" . implode("\n", $this->indentSqlCommandLines($formattedQueryLines));
    }

    /**
     * @param string[] $queryLines
     *
     * @return string[]
     */
    private function indentSqlCommandLines(array $queryLines): array
    {
        return array_map(function ($queryLine) {
            return str_repeat(self::INDENT_CHARACTERS, self::INDENT_COUNT) . $queryLine;
        }, $queryLines);
    }

    /**
     * @param string[] $sqlCommands
     *
     * @return string[]
     */
    private function escapeSqlCommands(array $sqlCommands): array
    {
        return array_map(function ($sqlCommand) {
            $escaped = str_replace('\'', "\\'", $sqlCommand);

            return '$this->addSql(\'' . $escaped . '\');';
        }, $sqlCommands);
    }
}
